{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Premières manipulations avec Python 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1/ Les variables\n",
    "\n",
    "Une **variable** est une sorte de petit tiroir dans lequel on range une donnée. On identifie ce tiroir par une étiquette qui est le **nom de la variable**. Cela permet de retrouver ce *tiroir* parmi tous les autres que constitue la mémoire.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Exemple :*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 8"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dans cet exemple, on a créé en mémoire un *tiroir* nommé 'a' qui contient l'entier 8."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  Convention d'écriture (les bonnes pratiques)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En règle générale, on se limitera aux caractères alphanumériques non accentués et au caractère de soulignement *underscore*. On prendra également soin de nommer les variables de façon 'lisible' afin de refléter le rôle joué par cette variable.\n",
    "\n",
    "Il s'agit uniquement d'une convention, ceci n'est pas imposé par le langage lui-même mais cela permet d'avoir une lecture claire de notre programme.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Exemples*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "age = 16\n",
    "taille_m = 1.8"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On voit bien ici que nos variables sont clairement identifiées et bien plus parlantes que dans l'exemple suivant :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = 16\n",
    "y = 1.8"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  2/ Les types de données"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Les variables peuvent être de plusieurs types possibles. Les plus fréquents en Python sont :"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table>\n",
    "<thead>\n",
    "<tr>\n",
    "<th align=\"left\">Type </th>\n",
    "<th align=\"center\"> Pour qui ?</th>\n",
    "<th align=\"right\"> Exemples </th>\n",
    "</tr>\n",
    "</thead>\n",
    "<tbody>\n",
    "<tr>\n",
    "<td align=\"left\"> int </td>\n",
    "<td align=\"center\"> Les entiers  </td>\n",
    "<td align=\"right\"> …, -5, -1, 0, 2, 6, … </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> float </td>\n",
    "<td align=\"center\"> Les réels </td>\n",
    "<td align=\"right\"> -5.28, 13.0, 18.7 </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> str </td>\n",
    "<td align=\"center\"> Les chaînes de caractères (texte) </td>\n",
    "<td align=\"right\"> ’Janvier’, ‘’28b6’’, ‘13’ </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> bool </td>\n",
    "<td align=\"center\"> Les booléens </td>\n",
    "<td align=\"right\"> uniquement deux valeurs possibles True (Vrai) ou False (Faux) </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> list </td>\n",
    "<td align=\"center\"> C’est une liste (ou tableau) dans lequel on peut stocker plusieurs valeurs idéalement de même type </td>\n",
    "<td align=\"right\"> [0, 1, 2, 4], [‘Janvier’, ‘Février’, ‘Mars’] </td>\n",
    "</tr>\n",
    "</tbody>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pour connaitre le type de la variable x, en python on utilise la fonction **type(x)**. Par exemple, la variable 'age' utilisée plus haut est bien de type entier :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "int"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(age)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  Attention au type !"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "str"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "chaine = \"16\"\n",
    "type(chaine)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ici la variable *chaine* contient la chaine de caractères constituée du caractère **'1'** suivi du caractère **'6'** et non de l'entier 16. Il faut donc être trés vigilant avec le type de notre variable !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  3/ L'affectation de variable"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "L'**affectation** est l'action qui permet l'attribution d'un contenu à une variable. C'est en fait le remplissage de notre *tiroir*. C'est cette instruction en python qui fixe le type de notre variable. On appelle cela en Python le typage dynamique."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Exemple :*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "pointure = 43\n",
    "longueur = 6.0\n",
    "condition_realisee = False\n",
    "nom = \"Tartampion\"\n",
    "liste_des_mois = [\"janvier\", \n",
    "                  \"février\",\n",
    "                  \"mars\",\n",
    "                  \"avril\",\n",
    "                  \"mai\",\n",
    "                  \"juin\",\n",
    "                  \"juillet\",\n",
    "                  \"août\",\n",
    "                  \"septembre\",\n",
    "                  \"octobre\",\n",
    "                  \"novembre\",\n",
    "                  \"décembre\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dans l'exemple précédent, nous avons affecté un contenu (dont le type correspond aux 5 principaux types Python vu en partie 2) à chacune de nos 5 variables. On notera le respect des bonnes pratiques concernant le nom de nos variables. Nous pouvons vérifier le typage *dynamique* de nos variables :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "int"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(pointure)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "float"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(longueur)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "bool"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(condition_realisee)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "str"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(nom)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "list"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "type(liste_des_mois)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  4/ Interactions avec l'utilisateur"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### La Saisie"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La plupart des scripts nécessitent à un moment donné une intervention de l'utilisateur. Il est alors invité à saisir **des caractères** au clavier. Cette opération se définit en Python avec la fonction **input()** comme dans l'exemple suivant. Cette fonction fournit en retour **une chaine de caractères** correspondant à ce que l'utilisateur a saisi. A l'intérieur des parenthèses, on donne un petit message descriptif destiné à l'utilisateur."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Veuillez saisir votre prénom : Kéké\n"
     ]
    }
   ],
   "source": [
    "prenom = input(\"Veuillez saisir votre prénom : \")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pour stocker en mémoire la saisie de l'utilisateur, on doit réaliser l'affectation de ce contenu à une variable (dans notre cas *prenom*). le typage dynamique de Python va nous donner un type **'str'**. Alors il faudra être vigilant lorsque l'on veut saisir des entiers ou des flottants (réels)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Veuillez saisir votre age : 39\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "str"
      ]
     },
     "execution_count": 13,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "age = input(\"Veuillez saisir votre age : \")\n",
    "type(age)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comme vu plus haut, ici notre variable age est de type 'str' alors que nous voulions au départ un entier ! \n",
    "\n",
    "Il faudra alors effectuer un changement de type pour faire comprendre à Python ce que nous voulons exactement. Et oui c'est le programmeur qui commande la machine. **Attention tout de même à ce changement de type, tout n'est pas convertible !**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Veuillez saisir votre age : 39\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "int"
      ]
     },
     "execution_count": 14,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "age = int(input(\"Veuillez saisir votre age : \"))\n",
    "type(age)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### L'affichage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Nous avons également souvent besoin d'afficher un certain nombre d'informations sur notre écran. Pour réaliser cette action avec Python, on utilise la fonction **print()** comme dans l'exemple suivant :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Je m'apelle Kéké\n",
      "et j'ai 39 ans\n"
     ]
    }
   ],
   "source": [
    "print(\"Je m'apelle\", prenom)\n",
    "print(\"et j'ai\", age, \"ans\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "##  5/ Les opérateurs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "On utilise des **opérateurs** (ce sont des symboles ou des mots réservés) pour manipuler les variables et leurs contenus."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Liste des principaux opérateurs arithmétiques (sur des 'int' ou 'float')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table>\n",
    "<thead>\n",
    "<tr>\n",
    "<th> Opérateurs</th>\n",
    "<th> Opérations effectuées</th>\n",
    "</tr>\n",
    "</thead>\n",
    "<tbody>\n",
    "<tr>\n",
    "<td> x + y </td>\n",
    "<td>Addition</td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td> x - y </td>\n",
    "<td> Soustraction </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td> x * y </td>\n",
    "<td> Multiplication </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td> x / y </td>\n",
    "<td> Division décimale de x par y </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td> x // y </td>\n",
    "<td> Quotient de la division euclidienne de x par y </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td> x % y </td>\n",
    "<td> Reste de la division euclidienne de x par y (modulo) </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td> x ** y </td>\n",
    "<td> Puissance : x à la puissance y </td>\n",
    "</tr>\n",
    "</tbody>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Exemples :*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "la somme de 3 + 4.2 = 7.2\n"
     ]
    }
   ],
   "source": [
    "x = 3\n",
    "y = 4.2\n",
    "somme = x + y\n",
    "print(\"la somme de\",x,\"+\",y,\"=\",somme )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "la différence de 3 - 4.0 = -1.0\n"
     ]
    }
   ],
   "source": [
    "x = 3\n",
    "y = 4.0\n",
    "difference = x - y\n",
    "print(\"la différence de\",x,\"-\",y,\"=\",difference )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "le produit de 3 * 21.0 = 63.0\n"
     ]
    }
   ],
   "source": [
    "x = 3\n",
    "y = 21.0\n",
    "produit = x * y\n",
    "print(\"le produit de\",x,\"*\",y,\"=\",produit )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "le quotient de la division décimale de 12 par 3.5 est 3.4285714285714284\n"
     ]
    }
   ],
   "source": [
    "x = 12\n",
    "y = 3.5\n",
    "quotient_decimal = x / y\n",
    "print(\"le quotient de la division décimale de\",x,\"par\",y,\"est\",quotient_decimal )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "le quotient de la division euclidienne de 12 par 5 est 2\n",
      "le reste de la division euclidienne de 12 par 5 est 2\n"
     ]
    }
   ],
   "source": [
    "x = 12\n",
    "y = 5\n",
    "quotient_euclidien = x // y\n",
    "reste = x % y\n",
    "print(\"le quotient de la division euclidienne de\",x,\"par\",y,\"est\",quotient_euclidien )\n",
    "print(\"le reste de la division euclidienne de\",x,\"par\",y,\"est\",reste )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "En effet 12 = 2 * 5 + 2 : Dividende = Quotient * Diviseur + Reste\n",
    "\n",
    "Il faut bien évidemment rappeler qu'une division euclidienne se réalise entre 2 entiers !"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "2 puissance 10 = 1024\n"
     ]
    }
   ],
   "source": [
    "x = 2\n",
    "y = 10\n",
    "x_puiss_y = x ** y\n",
    "print(x,\"puissance\",y,\"=\",x_puiss_y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Liste des principaux opérateurs sur des chaînes de caractères ('str')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table>\n",
    "<thead>\n",
    "<tr>\n",
    "<th align=\"left\"> Opérateurs</th>\n",
    "<th align=\"left\"> Opérations effectuées</th>\n",
    "</tr>\n",
    "</thead>\n",
    "<tbody>\n",
    "<tr>\n",
    "<td align=\"left\"> ch1 + ch2 </td>\n",
    "<td align=\"left\">Concaténation (les chaines ch1 et ch2 sont mises bout à bout)</td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> len(ch1) </td>\n",
    "<td align=\"left\"> Longueur de la chaine (renvoie le nombre de caractères de ch1) </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> ch1[i] </td>\n",
    "<td align=\"left\"> (i+1)ème caractère de ch1 </td>\n",
    "</tr>\n",
    "</tbody>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Exemples :*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Notre mot est azerty\n"
     ]
    }
   ],
   "source": [
    "ch1 = \"az\"\n",
    "ch2 = \"er\"\n",
    "ch3 = \"ty\"\n",
    "mot = ch1 + ch2 + ch3\n",
    "print(\"Notre mot est\",mot )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Le nombre de caractères dans azerty est : 6\n"
     ]
    }
   ],
   "source": [
    "print(\"Le nombre de caractères dans\",mot, \"est :\", len(mot) )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 38,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Le 3ième caractère de azerty est : e\n"
     ]
    }
   ],
   "source": [
    "print(\"Le 3ième caractère de\",mot, \"est :\", mot[2] )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Liste des principaux opérateurs sur des listes ('lst')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table>\n",
    "<thead>\n",
    "<tr>\n",
    "<th align=\"left\"> Opérateurs</th>\n",
    "<th align=\"left\"> Opérations effectuées</th>\n",
    "</tr>\n",
    "</thead>\n",
    "<tbody>\n",
    "<tr>\n",
    "<td align=\"left\"> len(L) </td>\n",
    "<td align=\"left\"> Donner le nombre d'élément présents dans la liste L</td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> L.append(e) </td>\n",
    "<td align=\"left\"> Ajouter l'élément e à la fin de la liste L </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> del L[i] </td>\n",
    "<td align=\"left\"> Supprimer le (i+1)ème élément de la liste L </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> L.reverse() </td>\n",
    "<td align=\"left\"> Inverser les élements de la liste L (le dernier élément sera le premier, etc.) </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> L.sort() </td>\n",
    "<td align=\"left\"> Trier la liste L dans l'ordre croissant </td>\n",
    "</tr>\n",
    "</tbody>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Il y a 3 éléments dans la liste suivante [1, 2, 3]\n"
     ]
    }
   ],
   "source": [
    "ma_liste = [1,2,3]\n",
    "print(\"Il y a\",len(ma_liste), \"éléments dans la liste suivante\", ma_liste)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 40,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "la nouvelle liste est [1, 2, 3, 4]\n"
     ]
    }
   ],
   "source": [
    "ma_liste.append(4)\n",
    "print(\"la nouvelle liste est\",ma_liste )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 42,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "la nouvelle liste est [1, 3, 4]\n"
     ]
    }
   ],
   "source": [
    "del ma_liste[1]\n",
    "print(\"la nouvelle liste est\",ma_liste )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 43,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "la liste inversée est [4, 3, 1]\n"
     ]
    }
   ],
   "source": [
    "ma_liste.reverse()\n",
    "print(\"la liste inversée est\",ma_liste )"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "la liste croissante est [1, 3, 4]\n"
     ]
    }
   ],
   "source": [
    "ma_liste.sort()\n",
    "print(\"la liste croissante est\",ma_liste )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Liste des principaux opérateurs de comparaison (pour former des expressions booléennes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table>\n",
    "<thead>\n",
    "<tr>\n",
    "<th align=\"left\"> Opérateurs</th>\n",
    "<th align=\"left\"> Opérations effectuées</th>\n",
    "</tr>\n",
    "</thead>\n",
    "<tbody>\n",
    "<tr>\n",
    "<td align=\"left\"> x > y </td>\n",
    "<td align=\"left\"> Test si x est supérieur à y et renvoi True dans ce cas et False sinon</td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> x >= y </td>\n",
    "<td align=\"left\"> Test si x est supérieur ou égal à y et renvoi True dans ce cas et False sinon </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> x < y </td>\n",
    "<td align=\"left\"> Test si x est inférieur à y et renvoi True dans ce cas et False sinon </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> x <= y </td>\n",
    "<td align=\"left\"> Test si x est inférieur ou égal à y et renvoi True dans ce cas et False sinon </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> x == y </td>\n",
    "<td align=\"left\"> Test si x est égal à y et renvoi True dans ce cas et False sinon </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> x != y </td>\n",
    "<td align=\"left\"> Test si x est différent à y et renvoi True dans ce cas et False sinon </td>\n",
    "</tr>\n",
    "</tbody>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n"
     ]
    }
   ],
   "source": [
    "print(7>3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n"
     ]
    }
   ],
   "source": [
    "print(14>=27)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n"
     ]
    }
   ],
   "source": [
    "print(7<3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n"
     ]
    }
   ],
   "source": [
    "print(14<=27)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "False\n"
     ]
    }
   ],
   "source": [
    "print(4.1==4)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n"
     ]
    }
   ],
   "source": [
    "print(9!=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Liste des principaux opérateurs logiques (pour réunir des expression booléennes)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<table>\n",
    "<thead>\n",
    "<tr>\n",
    "<th align=\"left\"> Opérateurs</th>\n",
    "<th align=\"left\"> Opérations effectuées</th>\n",
    "</tr>\n",
    "</thead>\n",
    "<tbody>\n",
    "<tr>\n",
    "<td align=\"left\"> x and y </td>\n",
    "<td align=\"left\">Intersection</td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> x or y </td>\n",
    "<td align=\"left\"> Union </td>\n",
    "</tr>\n",
    "<tr>\n",
    "<td align=\"left\"> not x </td>\n",
    "<td align=\"left\"> Négation </td>\n",
    "</tr>\n",
    "</tbody>\n",
    "</table>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "ici, x et y représentent des expressions booléennes :\n",
    "- x and y prend la valeur True si x et y sont True et sinon il prend la valeur False\n",
    "- x or y prend la valeur False si x et y sont False et sinon il prend la valeur True\n",
    "- not x prend la valeur True si x est False et prend la valeur False si x est True"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.1"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
